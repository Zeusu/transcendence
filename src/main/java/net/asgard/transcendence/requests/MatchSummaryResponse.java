package net.asgard.transcendence.requests;

import java.util.UUID;

import net.asgard.transcendence.entities.MatchSummary;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public class MatchSummaryResponse {

    private UUID summonerId;

    private long timestamp;

    private String gameId;

    private int kills;

    private int deaths;

    private int assists;

    private boolean win;

    private int championId;

    private long visionScore;

    private int goldEarned;

    private int[] items;

    private MatchSummaryResponse(final UUID summonerId, final long timestamp, final int kills, final int deaths,
            final int assists, final boolean win, final int championId, final long visionScore, final int goldEarned,
            final int[] items, final String gameId) {
        this.summonerId = summonerId;
        this.timestamp = timestamp;
        setGameId(gameId);
        this.kills = kills;
        this.deaths = deaths;
        this.assists = assists;
        this.win = win;
        this.championId = championId;
        this.visionScore = visionScore;
        this.goldEarned = goldEarned;
        this.items = items;
    }


    public MatchSummaryResponse(final MatchSummary summary) {
        this(summary.getId().getSummoner().getId(), summary.getId().getTimestamp(), summary.getKills(),
                summary.getDeaths(), summary.getAssists(), summary.isWin(), summary.getChampionId(),
                summary.getVisionScore(), summary.getGoldEarned(), summary.getItems(), summary.getGameId());
    }


    public UUID getSummonerId() {
        return summonerId;
    }

    public void setSummonerId(final UUID summonerId) {
        this.summonerId = summonerId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(final int kills) {
        this.kills = kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(final int deaths) {
        this.deaths = deaths;
    }

    public int getAssists() {
        return assists;
    }

    public void setAssists(final int assists) {
        this.assists = assists;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(final boolean win) {
        this.win = win;
    }

    public int getChampionId() {
        return championId;
    }

    public void setChampionId(final int championId) {
        this.championId = championId;
    }

    public long getVisionScore() {
        return visionScore;
    }

    public void setVisionScore(final long visionScore) {
        this.visionScore = visionScore;
    }

    public int getGoldEarned() {
        return goldEarned;
    }

    public void setGoldEarned(final int goldEarned) {
        this.goldEarned = goldEarned;
    }

    public int[] getItems() {
        return items;
    }

    public void setItems(final int[] items) {
        this.items = items;
    }


    public String getGameId() {
        return gameId;
    }


    public void setGameId(final String gameId) {
        this.gameId = gameId;
    }


}
