package net.asgard.transcendence.requests;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import net.asgard.transcendence.entities.Map;
import net.asgard.transcendence.entities.Zone;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = DefinedZoneDto.class, name = "DEFINED") })
public abstract class ZoneDto {

    private String name;

    private String color;

    public final Zone factory(final Map map) {
        return returnZone(createEntity(map), map);
    }

    public final Zone returnZone(final Zone z, final Map map) {
        z.setName(name);
        z.setColor(color);
        z.setMap(map);
        return z;
    }

    public abstract Zone createEntity(Map map);

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(final String color) {
        this.color = color;
    }

}
