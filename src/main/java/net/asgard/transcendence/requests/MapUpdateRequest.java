package net.asgard.transcendence.requests;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MapUpdateRequest {



    private Set<ZoneDto> zones;

    public Set<ZoneDto> getZones() {
        return zones;
    }

    public void setZones(final Set<ZoneDto> zones) {
        this.zones = zones;
    }

}
