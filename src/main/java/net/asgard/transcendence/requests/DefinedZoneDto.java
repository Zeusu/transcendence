package net.asgard.transcendence.requests;

import java.util.List;

import net.asgard.transcendence.entities.DefinedZone;
import net.asgard.transcendence.entities.Map;
import net.asgard.transcendence.entities.Point;
import net.asgard.transcendence.entities.Zone;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public class DefinedZoneDto extends ZoneDto {


    private List<Point> polygon;


    @Override
    public Zone createEntity(final Map map) {
        final DefinedZone zone = new DefinedZone();
        zone.setPolygon(polygon);
        return zone;
    }


    public List<Point> getPolygon() {
        return polygon;
    }


    public void setPolygon(final List<Point> polygon) {
        this.polygon = polygon;
    }


}
