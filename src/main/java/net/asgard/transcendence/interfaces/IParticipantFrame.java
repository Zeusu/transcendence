package net.asgard.transcendence.interfaces;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public interface IParticipantFrame extends RiotResponse {

    int getParticipantId();

    int getMinionsKilled();

    int getTotalGold();

    int getLevel();

    int getXp();

    int getCurrentGold();

    int getJungleMinionsKilled();

    IPosition getPosition();

}
