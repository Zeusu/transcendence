package net.asgard.transcendence.interfaces;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public interface IPosition extends RiotResponse {

    int getX();

    int getY();

}
