package net.asgard.transcendence.interfaces;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public interface IParticipant extends RiotResponse {

    public int getItem0();

    public int getItem1();

    public int getItem2();

    public int getItem3();

    public int getItem4();

    public int getItem5();

    public int getItem6();

    public int getKills();

    public int getDeaths();

    public int getAssists();

    public boolean isWin();

    public int getGoldEarned();

    public long getVisionScore();

    public int getChampionId();

    public int getParticipantId();

    public String getPuuid();

}
