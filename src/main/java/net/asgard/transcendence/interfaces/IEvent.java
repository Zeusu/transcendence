package net.asgard.transcendence.interfaces;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public interface IEvent extends RiotResponse {

    static enum Type {
        CHAMPION_KILL, WARD_PLACED, WARD_KILL, BUILDING_KILL, ELITE_MONSTER_KILL, ITEM_PURCHASED, ITEM_SOLD,
        ITEM_DESTROYED, ITEM_UNDO, SKILL_LEVEL_UP, ASCENDED_EVENT, CAPTURE_POINT, PORO_KING_SUMMON, PAUSE_END, LEVEL_UP,
        CHAMPION_SPECIAL_KILL, TURRET_PLATE_DESTROYED, GAME_END
    }

    long getTimestamp();

    Type getType();

    int getVictimId();

    int getKillerId();

    IPosition getPosition();

}
