package net.asgard.transcendence.interfaces;

import net.asgard.transcendence.entities.Summoner;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public interface IMatch extends RiotResponse {

    IParticipant getParticipant(Summoner s);

    String getGameId();

    long getGameCreation();

}
