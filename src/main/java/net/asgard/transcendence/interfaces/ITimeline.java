package net.asgard.transcendence.interfaces;

import java.util.List;
import java.util.Map;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public interface ITimeline extends RiotResponse {

    static interface Frame extends RiotResponse {

        Map<String, ? extends IParticipantFrame> getParticipantFrames();

        List<? extends IEvent> getEvents();

        IParticipantFrame findParticipant(int id);

        long getTimestamp();

    }

    long getFrameInterval();

    List<? extends Frame> getFrames();

}
