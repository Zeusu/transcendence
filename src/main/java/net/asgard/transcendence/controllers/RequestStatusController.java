package net.asgard.transcendence.controllers;

import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.asgard.transcendence.entities.RequestStatus;
import net.asgard.transcendence.entities.RequestStatus.Status;
import net.asgard.transcendence.repositories.RequestStatusRepository;

@RestController
@RequestMapping("requestStatus")
public class RequestStatusController {

    final RequestStatusRepository repository;

    public RequestStatusController(final RequestStatusRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/{id}")
    @Transactional
    public ResponseEntity<Object> getStatus(@PathVariable(name = "id") final UUID id) {

        Optional<RequestStatus> opt = repository.findById(id);

        if (opt.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Request doesn't exist");
        }

        final Status status = opt.get().getStatus();

        if (status == Status.COMPLETE || status == Status.ERROR) {
            repository.delete(opt.get());
        }


        return ResponseEntity.status(HttpStatus.OK).body(opt.get());
    }

}
