package net.asgard.transcendence.controllers;

import javax.transaction.Transactional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.asgard.transcendence.entities.Map;
import net.asgard.transcendence.repositories.MapRepository;
import net.asgard.transcendence.requests.MapUpdateRequest;
import net.asgard.transcendence.requests.ZoneDto;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
@RestController
@RequestMapping("map")
public class MapController {


    private final MapRepository repository;

    public MapController(final MapRepository repository) {
        this.repository = repository;
    }



    @GetMapping("/{name}")
    public Map getMap(@PathVariable(name = "name") final String mapKey) {
        return repository.findById(mapKey).get();
    }

    @PatchMapping("/{name}")
    @Transactional
    public ResponseEntity<Void> updateMap(@PathVariable(name = "name") final String name,
            @RequestBody final MapUpdateRequest request) {
        Map map = getMap(name);
        for (ZoneDto z : request.getZones()) {
            map.getZones().add(z.factory(map));
        }
        repository.save(map);
        return ResponseEntity.ok().build();
    }


}
