package net.asgard.transcendence.controllers;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.asgard.transcendence.entities.RequestStatus;
import net.asgard.transcendence.entities.Summoner;
import net.asgard.transcendence.repositories.SummonerRepository;
import net.asgard.transcendence.responses.SummonerResponse;
import net.asgard.transcendence.services.RiotLoLApiService;
import net.asgard.transcendence.services.riotlolapi.RequestPriority;
import net.asgard.transcendence.services.riotlolapi.dto.SummonerDto;
import net.asgard.transcendence.services.riotlolapi.requests.SummonerRequest;

@RestController
@RequestMapping("summoner")
public class SummonerController {

    private final RiotLoLApiService service;
    private final SummonerRepository repository;

    public SummonerController(final RiotLoLApiService service, final SummonerRepository repository) {
        this.service = service;
        this.repository = repository;
    }

    @PatchMapping("/{name}")
    public ResponseEntity<RequestStatus> patchSummoner(@PathVariable(name = "name") final String name) {

        final SummonerRequest request = new SummonerRequest(RequestPriority.HIGH, name);

        request.setCallback((final Object o) -> {

            final SummonerDto dto = (SummonerDto) o;
            final Summoner summoner = new Summoner(dto);


            final Optional<Summoner> summonerOpt = repository.findByAccountId(dto.getAccountId());

            if (summonerOpt.isPresent()) {
                summoner.setId(summonerOpt.get().getId());
            }

            repository.save(summoner);

        });

        service.send(request);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(request.getStatus());
    }


    @GetMapping("/{name}")
    public ResponseEntity<Object> getSummoner(@PathVariable(name = "name") final String name) {

        Optional<Summoner> summonerOpt = repository.findByName(name);

        if (summonerOpt.isEmpty()) {
            return ResponseEntity.status(HttpStatus.TOO_EARLY).build();
        }

        return ResponseEntity.ok(new SummonerResponse(summonerOpt.get()));
    }

}
