package net.asgard.transcendence.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.asgard.transcendence.services.RiotLoLApiService;

@RestController
@RequestMapping("admin/config")
public class ConfigController {

    private final RiotLoLApiService apiService;

    public ConfigController(final RiotLoLApiService apiService) {
        this.apiService = apiService;
    }

    @PatchMapping("/riot/{key}")
    public ResponseEntity<Void> updateRiotKey(@PathVariable(name = "key") final String key) {
        apiService.updateKey(key);
        return ResponseEntity.noContent().build();
    }

}
