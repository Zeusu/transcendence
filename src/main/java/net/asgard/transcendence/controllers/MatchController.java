package net.asgard.transcendence.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.asgard.transcendence.entities.MatchSummary;
import net.asgard.transcendence.entities.RequestStatus;
import net.asgard.transcendence.entities.Summoner;
import net.asgard.transcendence.repositories.MatchSummaryRepository;
import net.asgard.transcendence.repositories.SummonerRepository;
import net.asgard.transcendence.requests.MatchSummaryResponse;
import net.asgard.transcendence.services.MatchService;

@RestController
@RequestMapping("matches")
public class MatchController {

    private final MatchService service;
    private final MatchSummaryRepository repository;
    private final SummonerRepository sRepository;

    public MatchController(final MatchService service, final MatchSummaryRepository repository,
            final SummonerRepository sRepository) {
        this.service = service;
        this.repository = repository;
        this.sRepository = sRepository;
    }


    @PatchMapping("/{summonerId}")
    public ResponseEntity<Object> fetchMatches(@PathVariable(name = "summonerId") final UUID summonerId) {

        final Optional<Summoner> summonerOpt = sRepository.findById(summonerId);
        if (summonerOpt.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Summoner doesn't exist");
        }

        final RequestStatus state = service.fetchMatches(summonerOpt.get());

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(state);
    }

    @GetMapping("/{summonerId}")
    public ResponseEntity<Object> getMatches(@PathVariable(name = "summonerId") final UUID summonerId) {

        final Optional<Summoner> summonerOpt = sRepository.findById(summonerId);
        if (summonerOpt.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Summoner doesn't exist");
        }

        final List<MatchSummary> matches = repository.findAllById(summonerId);
        if (matches.size() == 0) {
            return ResponseEntity.status(HttpStatus.TOO_EARLY).build();
        }

        final List<MatchSummaryResponse> result = new ArrayList<>(matches.size());
        for (MatchSummary m : matches) {
            result.add(new MatchSummaryResponse(m));
        }
        return ResponseEntity.ok(result);
    }

}
