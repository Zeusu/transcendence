package net.asgard.transcendence.controllers;

import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import net.asgard.transcendence.entities.MatchAnalyze;
import net.asgard.transcendence.entities.MatchSummary;
import net.asgard.transcendence.repositories.MatchSummaryRepository;
import net.asgard.transcendence.services.RiotLoLApiService;
import net.asgard.transcendence.services.riotlolapi.Request;
import net.asgard.transcendence.services.riotlolapi.RequestPriority;
import net.asgard.transcendence.services.riotlolapi.RequestRetryable;
import net.asgard.transcendence.services.riotlolapi.dto.MatchTimelineDto;
import net.asgard.transcendence.services.riotlolapi.requests.MatchTimelineRequest;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
@RestController
@RequestMapping("analyze")
public class AnalyzeController {

    private final RiotLoLApiService lolService;
    private final MatchSummaryRepository sumRepository;

    public AnalyzeController(final RiotLoLApiService lolService, final MatchSummaryRepository sumRepository) {
        this.lolService = lolService;
        this.sumRepository = sumRepository;
    }

    @GetMapping("/{gameId}")
    public Object getAnalyze(@PathVariable(name = "gameId") final String gameId) {
        final Optional<MatchSummary> opt = sumRepository.findByGameId(gameId);

        if (opt.isEmpty()) {
            return null;
        }

        final MatchSummary summary = opt.get();
        final DeferredResult<Object> result = new DeferredResult<>();

        final Request request = new MatchTimelineRequest(RequestPriority.HIGH, RequestRetryable.ALWAYS, gameId);

        request.setCallback((final Object o) -> {
            final MatchAnalyze analyze = new MatchAnalyze(summary, (MatchTimelineDto) o);
            result.setResult(analyze);
        });

        lolService.send(request);

        return result;
    }

}
