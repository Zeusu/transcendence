package net.asgard.transcendence.services;

import org.springframework.stereotype.Service;

import net.asgard.transcendence.services.riotlolapi.Gate;
import net.asgard.transcendence.services.riotlolapi.Request;

@Service
public class RiotLoLApiService {

    private final Gate gate;

    public RiotLoLApiService(final Gate gate) {
        this.gate = gate;
        gate.setRunning(true);
        new Thread(gate).start();
    }

    public void send(final Request request) {
        gate.send(request);
    }

    public void updateKey(final String key) {
        gate.updateKey(key);
    }

}
