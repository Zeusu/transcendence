package net.asgard.transcendence.services.riotlolapi.dto;

import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.asgard.transcendence.entities.Summoner;
import net.asgard.transcendence.interfaces.IMatch;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MatchDto implements IMatch {


    public static final class MetadataDto {

        @JsonProperty("participants")
        private Set<String> puuids;

        private String matchId;

        public String getMatchId() {
            return matchId;
        }

        public void setMatchId(final String matchId) {
            this.matchId = matchId;
        }

    }

    public static final class InfoDto {

        private long gameCreation;

        private List<ParticipantDto> participants;

        public List<ParticipantDto> getParticipants() {
            return participants;
        }

        public void setParticipants(final List<ParticipantDto> participants) {
            this.participants = participants;
        }

        public long getGameCreation() {
            return gameCreation;
        }

        public void setGameCreation(final long gameCreation) {
            this.gameCreation = gameCreation;
        }

    }


    private MetadataDto metadata;

    private InfoDto info;


    @Override
    public ParticipantDto getParticipant(final Summoner summoner) {
        final String id = summoner.getRiotPuuid();
        for (ParticipantDto p : info.getParticipants()) {
            if (p.getPuuid().equals(id))
                return p;
        }
        return null;
    }


    public MetadataDto getMetadata() {
        return metadata;
    }


    public void setMetadata(final MetadataDto metadata) {
        this.metadata = metadata;
    }


    public InfoDto getInfo() {
        return info;
    }


    public void setInfo(final InfoDto info) {
        this.info = info;
    }

    @Override
    public String getGameId() {
        return metadata.matchId;
    }

    @Override
    public long getGameCreation() {
        return info.gameCreation;
    }


}
