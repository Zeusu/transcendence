package net.asgard.transcendence.services.riotlolapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import net.asgard.transcendence.interfaces.IParticipant;

@JsonIgnoreProperties
public class ParticipantDto implements IParticipant {



    private String puuid;
    private int championId;
    private int participantId;
    private int kills;
    private int deaths;
    private int assists;
    private int goldEarned;
    private long visionScore;
    private boolean win;
    private int item0;
    private int item1;
    private int item2;
    private int item3;
    private int item4;
    private int item5;
    private int item6;


    @Override
    public int getItem0() {
        return item0;
    }

    public void setItem0(final int item0) {
        this.item0 = item0;
    }

    @Override
    public int getItem1() {
        return item1;
    }

    public void setItem1(final int item1) {
        this.item1 = item1;
    }

    @Override
    public int getItem2() {
        return item2;
    }

    public void setItem2(final int item2) {
        this.item2 = item2;
    }

    @Override
    public int getItem3() {
        return item3;
    }

    public void setItem3(final int item3) {
        this.item3 = item3;
    }

    @Override
    public int getItem4() {
        return item4;
    }

    public void setItem4(final int item4) {
        this.item4 = item4;
    }

    @Override
    public int getItem5() {
        return item5;
    }

    public void setItem5(final int item5) {
        this.item5 = item5;
    }

    @Override
    public int getItem6() {
        return item6;
    }

    public void setItem6(final int item6) {
        this.item6 = item6;
    }

    @Override
    public int getKills() {
        return kills;
    }

    public void setKills(final int kills) {
        this.kills = kills;
    }

    @Override
    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(final int deaths) {
        this.deaths = deaths;
    }

    @Override
    public int getAssists() {
        return assists;
    }

    public void setAssists(final int assists) {
        this.assists = assists;
    }

    @Override
    public boolean isWin() {
        return win;
    }

    public void setWin(final boolean win) {
        this.win = win;
    }

    @Override
    public int getGoldEarned() {
        return goldEarned;
    }

    public void setGoldEarned(final int goldEarned) {
        this.goldEarned = goldEarned;
    }

    @Override
    public long getVisionScore() {
        return visionScore;
    }

    public void setVisionScore(final long visionScore) {
        this.visionScore = visionScore;
    }


    @Override
    public int getChampionId() {
        return championId;
    }


    public void setChampionId(final int championId) {
        this.championId = championId;
    }


    @Override
    public int getParticipantId() {
        return participantId;
    }

    public void setParticipantId(final int participantId) {
        this.participantId = participantId;
    }

    @Override
    public String getPuuid() {
        return puuid;
    }

    public void setPuuid(final String puuid) {
        this.puuid = puuid;
    }

}
