package net.asgard.transcendence.services.riotlolapi;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import net.asgard.transcendence.entities.RequestStatus.RequestError;
import net.asgard.transcendence.entities.RequestStatus.Status;
import net.asgard.transcendence.repositories.RequestStatusRepository;
import net.asgard.transcendence.repositories.RiotApiConfigRepository;
import net.asgard.transcendence.services.riotlolapi.entities.Region;
import net.asgard.transcendence.services.riotlolapi.entities.RiotApiConfig;


/**
 * @author Pierre
 *
 */
@Component
public final class Gate implements Runnable {


    private static final Set<HttpStatus> AUTHORIZED_STATUS_TO_RETRY = new HashSet<>(
            Arrays.asList(HttpStatus.TOO_MANY_REQUESTS, HttpStatus.GATEWAY_TIMEOUT));
    private static int requestCounter = 0;


    private final RiotApiConfigRepository configRep;
    private final RequestStatusRepository repository;
    private final RequestScheduler scheduler = new RequestScheduler();

    private final Queue secoundQueue;
    private final Queue minuteQueue;

    private final RestTemplate restTemplate;
    private final ThreadPoolExecutor threadPool = new ThreadPoolExecutor(4, 4, 5, TimeUnit.MINUTES,
            new LinkedBlockingQueue<>());


    private RiotApiConfig config;
    private boolean isRunning;



    public Gate(final RiotApiConfigRepository configRepository, final RestTemplateBuilder builder,
            final RequestStatusRepository repository) {

        restTemplate = builder.build();
        this.repository = repository;
        configRep = configRepository;

        final Optional<RiotApiConfig> configOpt = configRep.findById(Region.EUW);

        if (configOpt.isPresent()) {
            config = configOpt.get();
        } else {
            config = configRep.save(new RiotApiConfig(Region.EUW));
        }

        secoundQueue = new Queue(config.getSecoundInterval(), config.getMaxPerScound());
        minuteQueue = new Queue(config.getMinuteInterval(), config.getMaxPerMinute());

    }



    @Override
    public final void run() {

        while (isRunning) {

            final boolean hasPlaceInSecound = secoundQueue.hasPlace();
            final boolean hasPlaceInMinute = minuteQueue.hasPlace();
            final Request request;

            if (hasPlaceInSecound && hasPlaceInMinute && (request = scheduler.poll()) != null) {

                final boolean isBatchRequest = request instanceof BatchRequest;
                final Request treatRequest = isBatchRequest ? ((BatchRequest) request).poll() : request;

                if (treatRequest != null) {
                    secoundQueue.push(treatRequest);
                    minuteQueue.push(treatRequest);
                    threadPool.execute(() -> {
                        runRequest(treatRequest);
                    });
                }

                if (isBatchRequest && !request.isDeletable()) {
                    scheduler.add(request);
                } else if (isBatchRequest) {
                    request.getStatus().setStatus(Status.COMPLETE);
                    repository.save(request.getStatus());
                    if (request.getCallback() != null) {
                        threadPool.execute(() -> {
                            final BatchRequest br = (BatchRequest) request;
                            br.getCallback().handleResponse(br.whenFinished());
                        });
                    }
                }

            }

        }

    }



    public final void send(final Request request) {
        request.setStatus(repository.save(request.getStatus()));
        scheduler.add(request);
    }

    public final void updateKey(final String key) {
        config.setApiKey(key);
        config = configRep.save(config);
    }

    @PreDestroy
    public final void stop() {
        threadPool.shutdownNow();
        isRunning = false;
    }

    public final boolean isRunning() {
        return isRunning;
    }

    public final void setRunning(final boolean isRunning) {
        this.isRunning = isRunning;
    }




    /** CORE FUNCTION */
    private void runRequest(final Request request) {
        try {
            System.err.println("[" + requestCounter + "][" + System.currentTimeMillis() + "][Retryed: "
                    + request.retryed + "]: " + request);
            requestCounter = (requestCounter + 1) % Integer.MAX_VALUE;

            final HttpEntity<Void> httpEntity = new HttpEntity<>(config.getHeaders());

            request.setEmitedAt(System.currentTimeMillis());
            request.getStatus().setStatus(Status.SEND);
            request.setStatus(repository.save(request.getStatus()));

            final ResponseEntity<Object> rEntity = request.send(restTemplate, httpEntity);
            final Object response = rEntity.getBody();

            request.setRecievedAt(System.currentTimeMillis());

            request.getCallback().handleResponse(response);
            request.getStatus().setStatus(Status.COMPLETE);


        } catch (HttpStatusCodeException e) {

            e.printStackTrace();

            final HttpStatus code = e.getStatusCode();

            if (isOkToRetry(code, request)) {

                request.retry();
                scheduler.add(request);


            } else {

                request.getStatus().setStatus(Status.ERROR);
                request.getStatus().setError(chooseError(code));

            }

        } catch (Throwable t) {

            request.getStatus().setStatus(Status.ERROR);
            request.getStatus().setError(RequestError.UNKNOWN);
            t.printStackTrace();

        } finally {

            repository.save(request.getStatus());
        }

    }



    private final boolean isOkToRetry(final HttpStatus code, final Request rq) {
        return AUTHORIZED_STATUS_TO_RETRY.contains(code)
                && ((rq.getRetryable() == RequestRetryable.ONCE && rq.isRetryed() == false)
                        || rq.getRetryable() == RequestRetryable.ALWAYS);
    }

    private final RequestError chooseError(final HttpStatus httpCode) {
        final int code = httpCode.value();
        switch (code) {
            case 404:
                return RequestError.NOT_FOUND;

            case 400:
                return RequestError.BAD_REQUEST;

            case 403:
                return RequestError.KEY_EXPIRED;

        }

        if (AUTHORIZED_STATUS_TO_RETRY.contains(httpCode)) {
            return RequestError.TOO_MANY_TRY;
        }

        return RequestError.UNKNOWN;
    }

}
