package net.asgard.transcendence.services.riotlolapi.entities;

public enum GameQueue {

    RANKED(420);

    private final int code;

    private GameQueue(final int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

}
