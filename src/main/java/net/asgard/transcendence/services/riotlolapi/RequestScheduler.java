package net.asgard.transcendence.services.riotlolapi;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Pierre
 *
 */
public class RequestScheduler {

    private static final int HIGH_PRIORITY_BEFORE_MEDIUM = 3;
    private static final int MEDIUM_PRIORITY_BEFORE_LOW = 2;

    private final Queue<Request> highPriorityQueue = new LinkedList<>();
    private final Queue<Request> mediumPriorityQueue = new LinkedList<>();
    private final Queue<Request> lowPriorityQueue = new LinkedList<>();

    private int highProrityCounter = 0;
    private int mediumProrityCounter = 0;

    public void add(final Request r) {

        switch (r.getPriority()) {

            case SYSTEM:
                break;

            case HIGH:
                highPriorityQueue.add(r);
                break;

            case MEDIUM:
                mediumPriorityQueue.add(r);
                break;

            case LOW:
                lowPriorityQueue.add(r);
                break;

        }

    }

    public synchronized Request poll() {

        if (!highPriorityQueue.isEmpty() && highProrityCounter < HIGH_PRIORITY_BEFORE_MEDIUM) {
            highProrityCounter++;
            return highPriorityQueue.poll();
        } else if (!mediumPriorityQueue.isEmpty() && mediumProrityCounter < MEDIUM_PRIORITY_BEFORE_LOW) {
            highProrityCounter = 0;
            mediumProrityCounter++;
            return mediumPriorityQueue.poll();
        }

        highProrityCounter = 0;
        mediumProrityCounter = 0;

        if (!lowPriorityQueue.isEmpty()) {
            return lowPriorityQueue.poll();
        } else if (!highPriorityQueue.isEmpty()) {
            return highPriorityQueue.poll();
        } else if (!mediumPriorityQueue.isEmpty()) {
            return mediumPriorityQueue.poll();
        }

        return null;
    }

    public int size() {
        return lowPriorityQueue.size() + mediumPriorityQueue.size() + highPriorityQueue.size();
    }


}
