package net.asgard.transcendence.services.riotlolapi;

public enum RequestRetryable {
    NEVER, ONCE, ALWAYS
}
