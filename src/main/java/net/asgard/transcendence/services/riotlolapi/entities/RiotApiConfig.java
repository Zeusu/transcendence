package net.asgard.transcendence.services.riotlolapi.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@Entity
@Table(name = "riot_api_config")
public class RiotApiConfig {

    private static final long DEFAULT_SECOUND_INTERVAL = 1000;
    private static final int DEFAULT_SECOUND_MAX = 20 - 1;
    private static final long DEFAULT_MINUTE_INTERVAL = 1000 * 60 * 2;
    private static final int DEFAULT_MINUTE_MAX = 100 - 1;

    private static final String API_KEY_HEADER_NAME = "X-Riot-Token";
    private static final String DEFAULT_API_KEY = "RGAPI-1400d159-11f6-4c9a-8ce6-2bcf072a9279";


    @Id
    @Column(name = "region")
    @Enumerated(EnumType.STRING)
    private Region region;

    @Column(name = "secound_interval")
    private long secoundInterval = DEFAULT_SECOUND_INTERVAL;

    @Column(name = "max_per_secound")
    private int maxPerScound = DEFAULT_SECOUND_MAX;

    @Column(name = "minute_interval")
    private long minuteInterval = DEFAULT_MINUTE_INTERVAL;

    @Column(name = "max_pet_minute")
    private int maxPerMinute = DEFAULT_MINUTE_MAX;

    @Column(name = "api_key")
    private String apiKey = DEFAULT_API_KEY;


    @Transient
    private final HttpHeaders headers;


    public RiotApiConfig() {
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    public RiotApiConfig(final Region r) {
        this();
        region = r;
    }

    public HttpHeaders getHeaders() {
        headers.set(API_KEY_HEADER_NAME, apiKey);
        return headers;
    }

    public long getSecoundInterval() {
        return secoundInterval;
    }

    public void setSecoundInterval(final long secoundInterval) {
        this.secoundInterval = secoundInterval;
    }

    public int getMaxPerScound() {
        return maxPerScound;
    }

    public void setMaxPerScound(final int maxPerScound) {
        this.maxPerScound = maxPerScound;
    }

    public long getMinuteInterval() {
        return minuteInterval;
    }

    public void setMinuteInterval(final long minuteInterval) {
        this.minuteInterval = minuteInterval;
    }

    public int getMaxPerMinute() {
        return maxPerMinute;
    }

    public void setMaxPerMinute(final int maxPerMinute) {
        this.maxPerMinute = maxPerMinute;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(final String apiKey) {
        this.apiKey = apiKey;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(final Region region) {
        this.region = region;
    }


}
