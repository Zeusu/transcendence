package net.asgard.transcendence.services.riotlolapi;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import net.asgard.transcendence.entities.RequestStatus;
import net.asgard.transcendence.entities.RequestStatus.Status;
import net.asgard.transcendence.utils.URLFomatter;

public abstract class Request {

    @FunctionalInterface
    public static interface ResponseHandler {

        void handleResponse(Object response);

    }

    private final RequestRetryable retryable;

    protected RequestPriority priority;
    protected boolean retryed = false;
    protected ResponseHandler callback;

    private RequestStatus status = new RequestStatus(Status.IN_QUEUE);

    private long emitedAt;
    private long recievedAt;




    public Request(final RequestPriority p, final RequestRetryable r) {
        priority = p;
        retryable = r;
    }



    public abstract ResponseEntity<Object> send(final RestTemplate restTemplate, final HttpEntity<Void> entity);



    public void retry() {
        retryed = true;
    }

    public void markSend() {
        status.setStatus(Status.SEND);
    }

    public boolean isDeletable() {
        return true;
    }

    @Override
    public String toString() {
        return priority.toString();
    }



    @SuppressWarnings("unchecked")
    protected final ResponseEntity<Object> exec(final RestTemplate restTemplate, final URLFomatter url,
            final HttpEntity<Void> entity, final Class<?> cls) {
        final Object o = restTemplate.exchange(url.toString(), HttpMethod.GET, entity, cls);
        return (ResponseEntity<Object>) o;
    }



    public long getEmitedAt() {
        return emitedAt;
    }

    public void setEmitedAt(final long emitedAt) {
        this.emitedAt = emitedAt;
    }

    public long getRecievedAt() {
        return recievedAt;
    }

    public void setRecievedAt(final long recievedAt) {
        this.recievedAt = recievedAt;
    }


    public ResponseHandler getCallback() {
        return callback;
    }

    public void setCallback(final ResponseHandler callback) {
        this.callback = callback;
    }


    public RequestPriority getPriority() {
        return priority;
    }


    public RequestRetryable getRetryable() {
        return retryable;
    }

    public boolean isRetryed() {
        return retryed;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(final RequestStatus state) {
        status = state;
    }



}
