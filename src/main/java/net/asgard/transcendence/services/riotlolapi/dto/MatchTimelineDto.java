package net.asgard.transcendence.services.riotlolapi.dto;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import net.asgard.transcendence.interfaces.IEvent;
import net.asgard.transcendence.interfaces.IParticipantFrame;
import net.asgard.transcendence.interfaces.ITimeline;

@JsonIgnoreProperties(ignoreUnknown = true)


public class MatchTimelineDto implements ITimeline {

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MatchFrameDto implements ITimeline.Frame {

        private Map<String, MatchTimelineParticipantFrame> participantFrames;
        private List<MatchTimelineEventDto> events;
        private long timestamp;


        @Override
        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(final long timestamp) {
            this.timestamp = timestamp;
        }

        @Override
        public Map<String, ? extends IParticipantFrame> getParticipantFrames() {
            return participantFrames;
        }

        public void setParticipantFrames(final Map<String, MatchTimelineParticipantFrame> participantFrames) {
            this.participantFrames = participantFrames;
        }

        @Override
        public List<? extends IEvent> getEvents() {
            return events;
        }

        public void setEvents(final List<MatchTimelineEventDto> events) {
            this.events = events;
        }

        @Override
        public IParticipantFrame findParticipant(final int id) {
            for (IParticipantFrame p : participantFrames.values()) {
                if (p.getParticipantId() == id) {
                    return p;
                }
            }
            throw new RuntimeException("Participant does not exist");
        }

    }


    public static class InfoDto {
        private long frameInterval;
        private List<MatchFrameDto> frames;

        public long getFrameInterval() {
            return frameInterval;
        }

        public void setFrameInterval(final long frameInterval) {
            this.frameInterval = frameInterval;
        }

        public List<MatchFrameDto> getFrames() {
            return frames;
        }

        public void setFrames(final List<MatchFrameDto> frames) {
            this.frames = frames;
        }





    }


    private InfoDto info;


    public InfoDto getInfo() {
        return info;
    }


    public void setInfo(final InfoDto info) {
        this.info = info;
    }


    @Override
    public long getFrameInterval() {
        return info.frameInterval;
    }


    @Override
    public List<? extends Frame> getFrames() {
        return info.frames;
    }

}
