package net.asgard.transcendence.services.riotlolapi;

public class Queue {

    private final long interval;
    private final Request[] buffer;

    public Queue(final long interval, final int bufferLength) {
        this.interval = interval;
        buffer = new Request[bufferLength];

    }

    public synchronized void push(final Request request) {

        buffer[findPlace()] = request;

    }

    public synchronized boolean hasPlace() {

        boolean didClearSomething = false;
        for (int i = 0; i < buffer.length && !didClearSomething; i++) {

            final Request request = buffer[i];
            if (request == null || (System.currentTimeMillis() > request.getRecievedAt() + interval
                    && request.getRecievedAt() != 0)) {
                didClearSomething = true;
                buffer[i] = null;
            }

        }

        return didClearSomething;
    }

    public int size() {
        int size = 0;
        for (int i = 0; i < buffer.length; i++) {
            size = size + (buffer[i] != null ? 1 : 0);
        }
        return size;
    }

    private int findPlace() {

        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] == null) {
                return i;
            }
        }

        throw new RuntimeException("The buffer is full");
    }

}
