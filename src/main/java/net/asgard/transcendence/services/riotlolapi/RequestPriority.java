package net.asgard.transcendence.services.riotlolapi;

public enum RequestPriority {
    SYSTEM, HIGH, MEDIUM, LOW
}
