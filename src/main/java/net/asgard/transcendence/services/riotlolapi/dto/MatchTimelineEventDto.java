package net.asgard.transcendence.services.riotlolapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import net.asgard.transcendence.interfaces.IEvent;



/**
 * @author Pierre "Zeusu" Grenier
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatchTimelineEventDto implements IEvent {

    private long timestamp;
    private Type type;
    private int victimId;
    private int killerId;
    private MatchPosition position;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    public int getVictimId() {
        return victimId;
    }

    public void setVictimId(final int victimId) {
        this.victimId = victimId;
    }

    public int getKillerId() {
        return killerId;
    }

    public void setKillerId(final int killerId) {
        this.killerId = killerId;
    }

    public Type getType() {
        return type;
    }

    public void setType(final Type type) {
        this.type = type;
    }

    public MatchPosition getPosition() {
        return position;
    }

    public void setPosition(final MatchPosition position) {
        this.position = position;
    }

}
