package net.asgard.transcendence.services.riotlolapi.requests;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import net.asgard.transcendence.entities.RequestStatus.Status;
import net.asgard.transcendence.services.riotlolapi.BatchRequest;
import net.asgard.transcendence.services.riotlolapi.Request;
import net.asgard.transcendence.services.riotlolapi.RequestPriority;
import net.asgard.transcendence.services.riotlolapi.RequestRetryable;
import net.asgard.transcendence.services.riotlolapi.dto.MatchDto;
import net.asgard.transcendence.services.riotlolapi.entities.GameQueue;
import net.asgard.transcendence.utils.URLFomatter;
import net.asgard.transcendence.utils.URLFomatter.QueryParam;

public class MatchListRequest extends BatchRequest {

    public static final long NULL_TIME_VALUE = -1L;

    private static final String PUUID = "{puuid}";
    private static final String REQUEST_TARGET = "/match/v5/matches/by-puuid/" + PUUID + "/ids";
    private static final String QUEUE_PARAM = "queue";
    private static final String BEGIN_TIME_PARAM = "startTime";
    private static final String COUNT_PARAM = "count";

    private static final int COUNT_VALUE = 100;


    private final List<MatchDto> dtos;
    private final String accountId;
    private final GameQueue queue;
    private final long beginTime;
    private final Request listRequest;



    public MatchListRequest(final String accountId, final GameQueue queue, final long beginTime) {

        super(RequestPriority.HIGH);
        this.accountId = accountId;
        this.queue = queue;
        this.beginTime = beginTime;
        dtos = new ArrayList<>();


        listRequest = new Request(RequestPriority.HIGH, RequestRetryable.ALWAYS) {

            @Override
            public ResponseEntity<Object> send(final RestTemplate restTemplate, final HttpEntity<Void> entity) {
                final URLFomatter formatter = new URLFomatter(REQUEST_TARGET);
                formatter.replace(PUUID, accountId);
                formatter.addParam(new QueryParam(QUEUE_PARAM, "" + queue.getCode()));
                formatter.addParam(
                        new QueryParam(BEGIN_TIME_PARAM, beginTime > NULL_TIME_VALUE ? "" + beginTime : "" + 0));
                formatter.addParam(new QueryParam(COUNT_PARAM, "" + COUNT_VALUE));
                System.err.println(formatter);
                return exec(restTemplate, formatter, entity, ArrayList.class);
            }
        };

        listRequest.setCallback((final Object o) -> {
            @SuppressWarnings("unchecked")
            final ArrayList<String> ids = (ArrayList<String>) o;
            for (final String id : ids) {
                final Request r = new MatchRequest(RequestPriority.MEDIUM, id);
                r.setCallback((final Object o2) -> {
                    dtos.add((MatchDto) o2);
                });
                requests.add(r);
            }
        });

        requests.add(listRequest);
    }

    @Override
    protected Object whenFinished() {
        return dtos;
    }

    @Override
    public boolean isDeletable() {
        final Status status = listRequest.getStatus().getStatus();
        final boolean done = status == Status.COMPLETE || status == Status.ERROR;
        return done && super.isDeletable();
    }


    @Override
    public String toString() {
        return super.toString() + " " + accountId + "  " + REQUEST_TARGET + " " + queue + " " + beginTime;
    }

}
