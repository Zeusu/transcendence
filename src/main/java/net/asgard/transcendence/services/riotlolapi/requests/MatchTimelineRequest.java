package net.asgard.transcendence.services.riotlolapi.requests;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import net.asgard.transcendence.services.riotlolapi.Request;
import net.asgard.transcendence.services.riotlolapi.RequestPriority;
import net.asgard.transcendence.services.riotlolapi.RequestRetryable;
import net.asgard.transcendence.services.riotlolapi.dto.MatchTimelineDto;
import net.asgard.transcendence.utils.URLFomatter;

public class MatchTimelineRequest extends Request {

    private static final String TARGET_URL = "/match/v5/matches/";
    private static final String TIMELINE = "/timeline";

    private final String matchId;

    public MatchTimelineRequest(final RequestPriority p, final RequestRetryable r, final String matchId) {
        super(p, r);
        this.matchId = matchId;
    }

    @Override
    public ResponseEntity<Object> send(final RestTemplate restTemplate, final HttpEntity<Void> entity) {
        URLFomatter url = new URLFomatter(TARGET_URL);
        url.setExtension(matchId + TIMELINE);
        return exec(restTemplate, url, entity, MatchTimelineDto.class);
    }

    @Override
    public String toString() {
        return super.toString() + " " + TARGET_URL + " " + matchId;
    }

}
