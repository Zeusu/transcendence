package net.asgard.transcendence.services.riotlolapi.entities;

public enum Region {

    EUW, // EUROPE WEST
    NA, // NORTH AMERICA
    SK // SOUTH KOREA

}
