package net.asgard.transcendence.services.riotlolapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import net.asgard.transcendence.interfaces.IParticipantFrame;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MatchTimelineParticipantFrame implements IParticipantFrame {

    private int participantId;
    private int minionsKilled;
    private int totalGold;
    private int level;
    private int xp;
    private int currentGold;
    private int jungleMinionsKilled;
    private MatchPosition position;

    @Override
    public int getParticipantId() {
        return participantId;
    }

    public void setParticipantId(final int participantId) {
        this.participantId = participantId;
    }

    @Override
    public int getMinionsKilled() {
        return minionsKilled;
    }

    public void setMinionsKilled(final int minionsKilled) {
        this.minionsKilled = minionsKilled;
    }

    @Override
    public int getTotalGold() {
        return totalGold;
    }

    public void setTotalGold(final int totalGold) {
        this.totalGold = totalGold;
    }

    @Override
    public int getLevel() {
        return level;
    }

    public void setLevel(final int level) {
        this.level = level;
    }

    @Override
    public int getXp() {
        return xp;
    }

    public void setXp(final int xp) {
        this.xp = xp;
    }

    @Override
    public int getCurrentGold() {
        return currentGold;
    }

    public void setCurrentGold(final int currentGold) {
        this.currentGold = currentGold;
    }

    @Override
    public int getJungleMinionsKilled() {
        return jungleMinionsKilled;
    }

    public void setJungleMinionsKilled(final int jungleMinionsKilled) {
        this.jungleMinionsKilled = jungleMinionsKilled;
    }

    @Override
    public MatchPosition getPosition() {
        if (participantId == 9) {
            System.err.println("POS: " + position.getX() + ";" + position.getY());
        }
        return position;
    }

    public void setPosition(final MatchPosition position) {
        this.position = position;
    }



}
