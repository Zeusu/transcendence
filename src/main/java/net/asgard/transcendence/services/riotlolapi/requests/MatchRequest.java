package net.asgard.transcendence.services.riotlolapi.requests;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import net.asgard.transcendence.services.riotlolapi.Request;
import net.asgard.transcendence.services.riotlolapi.RequestPriority;
import net.asgard.transcendence.services.riotlolapi.RequestRetryable;
import net.asgard.transcendence.services.riotlolapi.dto.MatchDto;
import net.asgard.transcendence.utils.URLFomatter;

public class MatchRequest extends Request {

    private static final String REQUEST_TARGET = "/match/v5/matches";

    private final String matchId;

    public MatchRequest(final RequestPriority p, final String id) {
        super(p, RequestRetryable.ONCE);
        matchId = id;
    }

    @Override
    public ResponseEntity<Object> send(final RestTemplate restTemplate, final HttpEntity<Void> entity) {
        URLFomatter url = new URLFomatter(REQUEST_TARGET);
        url.setExtension(matchId);
        return exec(restTemplate, url, entity, MatchDto.class);
    }

    @Override
    public void retry() {
        super.retry();
        priority = RequestPriority.LOW;
    }

    @Override
    public String toString() {
        return super.toString() + " " + REQUEST_TARGET + " " + matchId;
    }

}
