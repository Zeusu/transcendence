package net.asgard.transcendence.services.riotlolapi;

import java.util.LinkedList;
import java.util.Queue;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import net.asgard.transcendence.entities.RequestStatus.Status;


/**
 * @author Pierre
 *
 */
public class BatchRequest extends Request {

    private static int brCounter = 0;
    private final int id;

    protected final Queue<Request> requests = new LinkedList<>();


    protected BatchRequest(final RequestPriority p) {
        super(p, RequestRetryable.NEVER);
        getStatus().setStatus(Status.SEND);
        id = brCounter;
        brCounter = (brCounter + 1) % Integer.MAX_VALUE;
    }

    protected Object whenFinished() {
        return null;
    }

    @Override
    public final ResponseEntity<Object> send(final RestTemplate restTemplate, final HttpEntity<Void> entity) {
        throw new Error("Can't send batch request directly");
    }

    @Override
    public boolean isDeletable() {
        final boolean deletable = requests.isEmpty();
        System.err.println(
                "[BR-" + id + "] Deletable: " + deletable + (deletable ? "" : (" | Still ") + requests.size()));
        if (deletable) {
            final Object result = whenFinished();
            if (result != null)
                callback.handleResponse(result);
        }
        return deletable;
    }

    @Override
    public RequestPriority getPriority() {
        return priority;
    }


    public void add(final Request r) {
        requests.add(r);
    }

    public Request poll() {
        return requests.poll();
    }



}
