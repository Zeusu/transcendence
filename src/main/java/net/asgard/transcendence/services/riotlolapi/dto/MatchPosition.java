package net.asgard.transcendence.services.riotlolapi.dto;

import net.asgard.transcendence.interfaces.IPosition;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public class MatchPosition implements IPosition {
    private int x;
    private int y;

    @Override
    public int getX() {
        return x;
    }

    public void setX(final int x) {
        this.x = x;
    }

    @Override
    public int getY() {
        return y;
    }

    public void setY(final int y) {
        this.y = y;
    }


}
