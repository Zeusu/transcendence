package net.asgard.transcendence.services.riotlolapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ParticipantIdentityDto {

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Player {

        private String accountId;
        private String summonerId;
        private String currentAccountId;

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(final String accountId) {
            this.accountId = accountId;
        }

        public String getSummonerId() {
            return summonerId;
        }

        public void setSummonerId(final String summonerId) {
            this.summonerId = summonerId;
        }

        public String getCurrentAccountId() {
            return currentAccountId;
        }

        public void setCurrentAccountId(final String currentAccountId) {
            this.currentAccountId = currentAccountId;
        }


    }

    private int participantId;

    private Player player;

    public int getParticipantId() {
        return participantId;
    }

    public void setParticipantId(final int participantId) {
        this.participantId = participantId;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(final Player player) {
        this.player = player;
    }

    public String getAccountId() {
        return player.getAccountId();
    }

    public String getSummonerId() {
        return player.getSummonerId();
    }

    public String getCurrentAccountId() {
        return player.getCurrentAccountId();
    }



}
