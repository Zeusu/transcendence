package net.asgard.transcendence.services.riotlolapi.requests;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import net.asgard.transcendence.services.riotlolapi.Request;
import net.asgard.transcendence.services.riotlolapi.RequestPriority;
import net.asgard.transcendence.services.riotlolapi.RequestRetryable;
import net.asgard.transcendence.services.riotlolapi.dto.SummonerDto;

public class SummonerRequest extends Request {

    private static final String SUMMONER_TARGET = "https://euw1.api.riotgames.com/lol/summoner/v4/summoners/";
    private static final String SUMMONER_BY_NAME_TARGET = SUMMONER_TARGET + "by-name/";

    private final String summonerName;

    public SummonerRequest(final RequestPriority priority, final String summonerName) {
        super(priority, RequestRetryable.ALWAYS);
        this.summonerName = summonerName;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ResponseEntity<Object> send(final RestTemplate restTemplate, final HttpEntity<Void> entity) {
        final String url = String.format("%s%s", SUMMONER_BY_NAME_TARGET, summonerName);
        final Object o = restTemplate.exchange(url, HttpMethod.GET, entity, SummonerDto.class);
        return (ResponseEntity<Object>) o;
    }

    @Override
    public String toString() {
        return super.toString() + " " + SUMMONER_BY_NAME_TARGET + " " + summonerName;
    }

}
