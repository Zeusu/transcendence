package net.asgard.transcendence.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import net.asgard.transcendence.entities.MatchSummary;
import net.asgard.transcendence.entities.RequestStatus;
import net.asgard.transcendence.entities.Summoner;
import net.asgard.transcendence.interfaces.IMatch;
import net.asgard.transcendence.repositories.MatchSummaryRepository;
import net.asgard.transcendence.services.riotlolapi.Request;
import net.asgard.transcendence.services.riotlolapi.dto.MatchDto;
import net.asgard.transcendence.services.riotlolapi.entities.GameQueue;
import net.asgard.transcendence.services.riotlolapi.requests.MatchListRequest;


/**
 * @author Pierre
 *
 */
@Service
public class MatchService {

    private final RiotLoLApiService lolApiService;
    final MatchSummaryRepository repository;

    public MatchService(final RiotLoLApiService lolApiService, final MatchSummaryRepository repository) {
        this.lolApiService = lolApiService;
        this.repository = repository;
    }

    public RequestStatus fetchMatches(final Summoner summoner) {

        final Optional<Long> optMaxTimestamp = repository.findMaxTimestampForSummonerId(summoner.getId());
        final long start = optMaxTimestamp.isPresent() ? optMaxTimestamp.get() : MatchListRequest.NULL_TIME_VALUE;

        final Request r = new MatchListRequest(summoner.getRiotPuuid(), GameQueue.RANKED, start);

        r.setCallback((final Object o) -> {
            @SuppressWarnings("unchecked")
            final List<MatchDto> list = (List<MatchDto>) o;
            final List<MatchSummary> summaries = new ArrayList<>();
            for (final IMatch dto : list) {
                final MatchSummary m = new MatchSummary(summoner, dto, dto.getGameCreation());
                summaries.add(m);
            }
            repository.saveAll(summaries);
        });

        lolApiService.send(r);

        return r.getStatus();
    }


}
