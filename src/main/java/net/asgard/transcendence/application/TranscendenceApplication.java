package net.asgard.transcendence.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableAsync
@EntityScan({ "net.asgard.transcendence.entities", "net.asgard.transcendence.services.riotlolapi.entities" })
@EnableJpaRepositories(basePackages = "net.asgard.transcendence.repositories")
@EnableTransactionManagement
@EnableJpaAuditing
@ComponentScan(basePackages = "net.asgard.transcendence")
@EnableAutoConfiguration
public class TranscendenceApplication {

    public static void main(final String[] args) {
        SpringApplication.run(TranscendenceApplication.class, args);
    }


}
