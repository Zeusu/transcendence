package net.asgard.transcendence.responses;

import java.util.UUID;

import net.asgard.transcendence.entities.Summoner;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public class SummonerResponse {

    private UUID id;
    private String name;
    private int profileIconId;
    private long level;

    public SummonerResponse(final Summoner summoner) {
        id = summoner.getId();
        name = summoner.getName();
        profileIconId = summoner.getProfileIconId();
        level = summoner.getSummonerLevel();
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public long getLevel() {
        return level;
    }

    public void setLevel(final long level) {
        this.level = level;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getProfileIconId() {
        return profileIconId;
    }

    public void setProfileIconId(final int profileIconId) {
        this.profileIconId = profileIconId;
    }


}
