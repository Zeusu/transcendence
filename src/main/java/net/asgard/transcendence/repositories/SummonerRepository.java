package net.asgard.transcendence.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.asgard.transcendence.entities.Summoner;

@Repository
public interface SummonerRepository extends CrudRepository<Summoner, UUID> {

    @Query("SELECT s FROM Summoner s WHERE s.riotAccountId = ?1")
    public Optional<Summoner> findByAccountId(String accountId);

    @Query("SELECT s FROM Summoner s WHERE s.name = ?1")
    public Optional<Summoner> findByName(String name);

}
