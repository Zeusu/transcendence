package net.asgard.transcendence.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import net.asgard.transcendence.entities.RequestStatus;

public interface RequestStatusRepository extends CrudRepository<RequestStatus, UUID> {

}
