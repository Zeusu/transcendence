package net.asgard.transcendence.repositories;

import org.springframework.data.repository.CrudRepository;

import net.asgard.transcendence.services.riotlolapi.entities.Region;
import net.asgard.transcendence.services.riotlolapi.entities.RiotApiConfig;

public interface RiotApiConfigRepository extends CrudRepository<RiotApiConfig, Region> {

}
