package net.asgard.transcendence.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import net.asgard.transcendence.entities.MatchId;
import net.asgard.transcendence.entities.MatchSummary;

public interface MatchSummaryRepository extends CrudRepository<MatchSummary, MatchId> {

    @Query("SELECT MAX(m.id.timestamp) FROM MatchSummary m WHERE m.id.summoner.id = ?1")
    public Optional<Long> findMaxTimestampForSummonerId(UUID summonerId);


    @Query("SELECT m FROM MatchSummary m WHERE m.id.summoner.id = ?1 ORDER BY m.id.timestamp DESC")
    public List<MatchSummary> findAllById(UUID summonerId);


    @Query("SELECT m FROM MatchSummary m WHERE m.gameId = ?1")
    public Optional<MatchSummary> findByGameId(String gameId);
}
