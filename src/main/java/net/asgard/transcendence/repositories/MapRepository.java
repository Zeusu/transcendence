package net.asgard.transcendence.repositories;

import org.springframework.data.repository.CrudRepository;

import net.asgard.transcendence.entities.Map;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public interface MapRepository extends CrudRepository<Map, String> {

}
