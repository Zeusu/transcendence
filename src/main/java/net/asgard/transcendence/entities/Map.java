package net.asgard.transcendence.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
@Entity
@Table(name = "map")
public class Map {


    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "riot_id", unique = true)
    private int riotId;

    @Column(name = "height")
    private int height;

    @Column(name = "width")
    private int width;

    @OneToMany(mappedBy = "map", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Zone> zones;


    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getRiotId() {
        return riotId;
    }

    public void setRiotId(final int riotId) {
        this.riotId = riotId;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(final int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(final int width) {
        this.width = width;
    }

    public Set<Zone> getZones() {
        return zones;
    }

    public void setZones(final Set<Zone> zones) {
        this.zones = zones;
    }


}
