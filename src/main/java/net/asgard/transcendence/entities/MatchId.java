package net.asgard.transcendence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
@Embeddable
public class MatchId implements Serializable {

    private static final long serialVersionUID = -5682708389721285525L;

    public MatchId() {
    }

    public MatchId(final Summoner summoner, final long timestamp) {
        this.summoner = summoner;
        this.timestamp = timestamp;
    }

    @ManyToOne
    @JoinColumn(name = "summoner_id")
    private Summoner summoner;

    @Column(name = "timestamp")
    private long timestamp;

    public Summoner getSummoner() {
        return summoner;
    }

    public void setSummoner(final Summoner summoner) {
        this.summoner = summoner;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

}