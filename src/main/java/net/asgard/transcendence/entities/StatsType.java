package net.asgard.transcendence.entities;

public enum StatsType {

    SUMMONER, CHAMPION, ROLE;

}
