package net.asgard.transcendence.entities;

import net.asgard.transcendence.interfaces.IEvent;
import net.asgard.transcendence.interfaces.IParticipantFrame;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public enum AnalyzeEventType {

    DEATH((final IEvent e, final int participantId) -> (e.getVictimId() == participantId),
            (final IParticipantFrame p, final IEvent event, final AnalyzeEvent aEvent) -> {
                aEvent.setXY(event.getPosition());
                aEvent.setKiller(event.getKillerId());
            }),

    KILL((final IEvent e, final int participantId) -> (e.getKillerId() == participantId),
            (final IParticipantFrame p, final IEvent event, final AnalyzeEvent aEvent) -> {
                aEvent.setXY(event.getPosition());
                aEvent.setVictim(event.getVictimId());
            });

    @FunctionalInterface
    private static interface Filter {
        public boolean use(IEvent e, int participantId);
    }

    @FunctionalInterface
    private static interface Factory {
        public void fill(final IParticipantFrame p, final IEvent event, final AnalyzeEvent aEvent);
    }

    private final Filter filter;
    private final Factory factory;

    private AnalyzeEventType(final Filter filter, final Factory factory) {
        this.filter = filter;
        this.factory = factory;
    }

    public boolean filter(final IEvent e, final int participantId) {
        return filter.use(e, participantId);
    }

    public AnalyzeEvent create(final IParticipantFrame p, final IEvent e, final long timestamp) {
        final AnalyzeEvent aEvent = new AnalyzeEvent(this, timestamp);
        factory.fill(p, e, aEvent);
        return aEvent;
    }

}
