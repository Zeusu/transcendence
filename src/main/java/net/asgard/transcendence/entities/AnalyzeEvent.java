package net.asgard.transcendence.entities;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import net.asgard.transcendence.interfaces.IPosition;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
@JsonInclude(value = Include.NON_NULL)
@Entity
@Table(name = "analyze_event")
public final class AnalyzeEvent {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private MatchAnalyze analyze;

    @Column(name = "type")
    private AnalyzeEventType type;

    @Column(name = "timestamp")
    private long timestamp;

    @Column(name = "killer")
    private Integer killer;

    @Column(name = "victim")
    private Integer victim;

    @Column(name = "x")
    private int x;

    @Column(name = "y")
    private int y;

    public AnalyzeEvent() {
    }

    protected AnalyzeEvent(final AnalyzeEventType type, final long timestamp) {
        this.type = type;
        this.timestamp = timestamp;
    }

    public void setXY(final IPosition pos) {
        x = pos.getX();
        y = pos.getY();
    }


    public Integer getKiller() {
        return killer;
    }

    public void setKiller(final Integer killer) {
        this.killer = killer;
    }


    public int getX() {
        return x;
    }

    public void setX(final int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(final int y) {
        this.y = y;
    }

    public Integer getVictim() {
        return victim;
    }

    public void setVictim(final Integer victim) {
        this.victim = victim;
    }

    public MatchAnalyze getAnalyze() {
        return analyze;
    }

    public void setAnalyze(final MatchAnalyze analyze) {
        this.analyze = analyze;
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public AnalyzeEventType getType() {
        return type;
    }

    public void setType(final AnalyzeEventType type) {
        this.type = type;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }


}
