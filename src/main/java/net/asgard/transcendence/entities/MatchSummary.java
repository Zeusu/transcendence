package net.asgard.transcendence.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import net.asgard.transcendence.interfaces.IMatch;
import net.asgard.transcendence.interfaces.IParticipant;


/**
 * @author Pierre "Zeusu" Grenier
 *
 */
@Entity
@Table(name = "match_summary", indexes = {
        @Index(name = "summoner_id_index", columnList = "summoner_id", unique = false),
        @Index(name = "game_id_index", columnList = "game_id", unique = true) })
public class MatchSummary {

    @Id
    private MatchId id;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = true, orphanRemoval = true)
    private MatchAnalyze analyze;

    @Column(name = "game_id", nullable = false, unique = true)
    private String gameId;

    @Column(name = "participant_id")
    private int participantId;

    @Column(name = "kills")
    private int kills;

    @Column(name = "deaths")
    private int deaths;

    @Column(name = "assists")
    private int assists;

    @Column(name = "win")
    private boolean win;

    @Column(name = "champion_id")
    private int championId;

    @Column(name = "vision_score")
    private long visionScore;

    @Column(name = "gold_earned")
    private int goldEarned;

    @Column(name = "item0")
    private int item0;
    @Column(name = "item1")
    private int item1;
    @Column(name = "item2")
    private int item2;
    @Column(name = "item3")
    private int item3;
    @Column(name = "item4")
    private int item4;
    @Column(name = "item5")
    private int item5;
    @Column(name = "item6")
    private int item6;

    public MatchSummary() {
    }

    public MatchSummary(final Summoner summoner, final IMatch dto, final long timestamp) {
        this();
        id = new MatchId(summoner, timestamp);
        final IParticipant p = dto.getParticipant(summoner);
        gameId = dto.getGameId();
        setParticipantId(p.getParticipantId());
        kills = p.getKills();
        assists = p.getAssists();
        deaths = p.getDeaths();
        championId = p.getChampionId();
        win = p.isWin();
        visionScore = p.getVisionScore();
        goldEarned = p.getGoldEarned();
        item0 = p.getItem0();
        item1 = p.getItem1();
        item2 = p.getItem2();
        item3 = p.getItem3();
        item4 = p.getItem4();
        item5 = p.getItem5();
        item6 = p.getItem6();
    }


    @Transient
    public int[] getItems() {
        return new int[] { item0, item1, item2, item3, item4, item5, item6 };
    }



    public long getVisionScore() {
        return visionScore;
    }

    public void setVisionScore(final long visionScore) {
        this.visionScore = visionScore;
    }

    public int getGoldEarned() {
        return goldEarned;
    }

    public void setGoldEarned(final int goldEarned) {
        this.goldEarned = goldEarned;
    }

    public int getItem0() {
        return item0;
    }

    public void setItem0(final int item0) {
        this.item0 = item0;
    }

    public int getItem1() {
        return item1;
    }

    public void setItem1(final int item1) {
        this.item1 = item1;
    }

    public int getItem2() {
        return item2;
    }

    public void setItem2(final int item2) {
        this.item2 = item2;
    }

    public int getItem3() {
        return item3;
    }

    public void setItem3(final int item3) {
        this.item3 = item3;
    }

    public int getItem4() {
        return item4;
    }

    public void setItem4(final int item4) {
        this.item4 = item4;
    }

    public int getItem5() {
        return item5;
    }

    public void setItem5(final int item5) {
        this.item5 = item5;
    }

    public int getItem6() {
        return item6;
    }

    public void setItem6(final int item6) {
        this.item6 = item6;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(final int kills) {
        this.kills = kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(final int deaths) {
        this.deaths = deaths;
    }

    public int getAssists() {
        return assists;
    }

    public void setAssists(final int assists) {
        this.assists = assists;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(final boolean win) {
        this.win = win;
    }

    public MatchId getId() {
        return id;
    }

    public void setId(final MatchId id) {
        this.id = id;
    }

    public int getChampionId() {
        return championId;
    }

    public void setChampionId(final int championId) {
        this.championId = championId;
    }

    public int getParticipantId() {
        return participantId;
    }

    public void setParticipantId(final int participantId) {
        this.participantId = participantId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(final String gameId) {
        this.gameId = gameId;
    }

    public MatchAnalyze getAnalyze() {
        return analyze;
    }

    public void setAnalyze(final MatchAnalyze analyze) {
        this.analyze = analyze;
    }

}
