package net.asgard.transcendence.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Zone {

    @Id
    @Column(name = "name")
    protected String name;

    @Column(name = "color", length = 9)
    private String color;

    @Transient
    @JsonProperty
    private final ZoneType type;

    @JsonIgnore
    @ManyToOne(optional = false)
    private Map map;

    protected Zone(final ZoneType t) {
        type = t;
    }


    public abstract boolean isInZone(Point p);


    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        return name.equals(((Zone) obj).name);
    }

    public Map getMap() {
        return map;
    }

    public void setMap(final Map map) {
        this.map = map;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(final String color) {
        this.color = color;
    }

    public ZoneType getType() {
        return type;
    }


}
