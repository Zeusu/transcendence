package net.asgard.transcendence.entities;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import net.asgard.transcendence.services.riotlolapi.dto.SummonerDto;

@Entity
@Table(name = "summoner", indexes = { @Index(name = "name_index", columnList = "name"),
        @Index(name = "riot_account_id_index", columnList = "riot_account_id", unique = true) })
public class Summoner {

    public Summoner() {
    }

    public Summoner(final SummonerDto dto) {
        name = dto.getName();
        riotAccountId = dto.getAccountId();
        profileIconId = dto.getProfileIconId();
        revisionDate = dto.getRevisionDate();
        riotId = dto.getId();
        riotPuuid = dto.getPuuid();
        summonerLevel = dto.getSummonerLevel();
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "riot_account_id", unique = true, nullable = false)
    private String riotAccountId;

    @Column(name = "profile_icon_id")
    private int profileIconId;

    @Column(name = "revision_date")
    private long revisionDate;

    @Column(name = "riot_id", unique = true, nullable = false)
    private String riotId;

    @Column(name = "riot_puuid", unique = true, nullable = false)
    private String riotPuuid;

    @Column(name = "summoner_level")
    private long summonerLevel;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public int getProfileIconId() {
        return profileIconId;
    }

    public void setProfileIconId(final int profileIconId) {
        this.profileIconId = profileIconId;
    }

    public long getRevisionDate() {
        return revisionDate;
    }

    public void setRevisionDate(final long revisionDate) {
        this.revisionDate = revisionDate;
    }

    public String getRiotId() {
        return riotId;
    }

    public void setRiotId(final String riotId) {
        this.riotId = riotId;
    }

    public String getRiotPuuid() {
        return riotPuuid;
    }

    public void setRiotPuuid(final String riotPuuid) {
        this.riotPuuid = riotPuuid;
    }

    public long getSummonerLevel() {
        return summonerLevel;
    }

    public void setSummonerLevel(final long summonerLevel) {
        this.summonerLevel = summonerLevel;
    }

    public String getRiotAccountId() {
        return riotAccountId;
    }

    public void setRiotAccountId(final String riotAccountId) {
        this.riotAccountId = riotAccountId;
    }

}
