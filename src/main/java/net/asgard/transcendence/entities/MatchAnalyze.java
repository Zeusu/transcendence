package net.asgard.transcendence.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import net.asgard.transcendence.interfaces.IEvent;
import net.asgard.transcendence.interfaces.IEvent.Type;
import net.asgard.transcendence.interfaces.IParticipantFrame;
import net.asgard.transcendence.interfaces.ITimeline;
import net.asgard.transcendence.interfaces.ITimeline.Frame;
import net.asgard.transcendence.services.riotlolapi.dto.MatchTimelineDto;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
@Entity
@Table(name = "match_analyze")
public class MatchAnalyze {

    @Id
    private MatchId id;

    @OneToOne(optional = false, fetch = FetchType.LAZY, orphanRemoval = false)
    private MatchSummary summary;

    @Transient
    private static final Set<Type> TREATED_EVENTS = Set.of(Type.CHAMPION_KILL);

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "analyze_summoner_id", referencedColumnName = "summoner_id")
    @JoinColumn(name = "analyze_timestamp", referencedColumnName = "timestamp")
    private List<AnalyzeEvent> events;


    public MatchAnalyze() {
    }

    public MatchAnalyze(final MatchSummary summary, final MatchTimelineDto timeline) {
        final int participantId = summary.getParticipantId();
        events = new ArrayList<>();
        filter(timeline, participantId);
    }


    public List<AnalyzeEvent> getEvents() {
        return events;
    }

    public void setEvents(final List<AnalyzeEvent> events) {
        this.events = events;
    }

    private void filter(final ITimeline dto, final int participantId) {


        for (Frame frame : dto.getFrames()) {

            final IParticipantFrame p = frame.findParticipant(participantId);
            for (IEvent e : frame.getEvents()) {

                if (TREATED_EVENTS.contains(e.getType())) {

                    for (AnalyzeEventType t : AnalyzeEventType.values()) {

                        if (t.filter(e, participantId)) {
                            events.add(t.create(p, e, e.getTimestamp()));
                        }

                    }

                }

            }

        }

    }

    public MatchSummary getSummary() {
        return summary;
    }

    public void setSummary(final MatchSummary summary) {
        this.summary = summary;
    }
}
