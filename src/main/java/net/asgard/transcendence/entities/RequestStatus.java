package net.asgard.transcendence.entities;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "request_status")
public class RequestStatus {

    public static enum Status {
        IN_QUEUE, SEND, COMPLETE, ERROR
    }

    public static enum RequestError {
        NONE, BAD_REQUEST, NOT_FOUND, KEY_EXPIRED, TOO_MANY_TRY, UNKNOWN
    }

    @Id
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Enumerated(EnumType.STRING)
    @Column(name = "error")
    private RequestError error = RequestError.NONE;

    public RequestStatus() {
    }

    public RequestStatus(final Status state) {
        status = state;
        id = UUID.randomUUID();
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status state) {
        status = state;
    }

    public RequestError getError() {
        return error;
    }

    public void setError(final RequestError error) {
        this.error = error;
    }


}
