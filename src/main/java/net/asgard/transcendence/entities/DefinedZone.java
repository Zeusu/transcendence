package net.asgard.transcendence.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import net.asgard.transcendence.utils.MathUtils;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
@Entity
@Table(name = "defined_zone")
public class DefinedZone extends Zone {


    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "zone_name", referencedColumnName = "name")
    @OrderColumn(name = "index")
    private List<Point> polygon;

    public DefinedZone() {
        super(ZoneType.DEFINED);
    }

    @Override
    public boolean isInZone(final Point p) {
        double totalAg = 0.0;
        for (int i = 0; i < polygon.size(); i++) {
            totalAg += MathUtils.revertPythagore(p, polygon.get(i), polygon.get((i + 1) % polygon.size()));
        }
        return Math.round(totalAg) == 360;
    }


    public List<Point> getPolygon() {
        return polygon;
    }

    public void setPolygon(final List<Point> polygon) {
        this.polygon = polygon;
    }


}
