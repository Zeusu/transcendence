package net.asgard.transcendence.utils;

import org.springframework.stereotype.Component;

import net.asgard.transcendence.entities.Map;
import net.asgard.transcendence.repositories.MapRepository;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
@Component
public final class DefaultValues {

    public static final String SUMMONER_RIFT_NAME = "summoner_rift";
    public static final int SUMMONER_RIFT_SIZE = 15000;
    public static final int SUMMONER_RIF_ID = 1;

    private final MapRepository repository;

    public DefaultValues(final MapRepository repository) {
        this.repository = repository;
        init();
    }

    private void init() {
        initMap();
    }

    private void initMap() {
        final Map summonerRif = new Map();
        summonerRif.setName(SUMMONER_RIFT_NAME);
        summonerRif.setHeight(SUMMONER_RIFT_SIZE);
        summonerRif.setWidth(SUMMONER_RIFT_SIZE);
        summonerRif.setRiotId(SUMMONER_RIF_ID);
        repository.save(summonerRif);
    }

}
