package net.asgard.transcendence.utils;

import net.asgard.transcendence.entities.Point;

/**
 * @author Pierre "Zeusu" Grenier
 *
 */
public final class MathUtils {

    /**
     * Search B�C
     *
     * @param a a point at the top of the angle
     * @param b a point of the angle
     * @param c a point of the angle
     * @returns arccos(cos(�))
     */
    public static final double revertPythagore(final Point a, final Point b, final Point c) {
        final double ab = pythagore(a, b);
        final double bc = pythagore(b, c);
        final double ca = pythagore(c, a);
        final double cosA = (sq(ab) + sq(ca) - sq(bc)) / (2 * ab * ca);
        return arccos(cosA);
    }

    public static final double pythagore(final Point a, final Point b) {
        return Math.sqrt(sq(a.getX() - b.getX()) + sq(a.getY() - b.getY()));
    }

    public static final double arccos(final double cos) {
        return Math.acos(cos) * (180 / Math.PI);
    }

    public static final double sq(final double x) {
        return x * x;
    }


}
