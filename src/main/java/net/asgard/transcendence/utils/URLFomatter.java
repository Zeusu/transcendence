package net.asgard.transcendence.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class URLFomatter {

    public static final String RIOT_EUW1_LOL_BASE_URL = "https://europe.api.riotgames.com/lol";

    public static class QueryParam {
        private String name;
        private String value;

        public QueryParam(final String name, final String value) {
            this.name = name;
            this.value = value;
        }
    }

    private String baseUrl;
    private String extension = null;
    private List<QueryParam> params = new ArrayList<>();

    public URLFomatter(final String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer(RIOT_EUW1_LOL_BASE_URL + baseUrl);

        if (extension != null) {
            buffer.append('/');
            buffer.append(extension);
        }

        if (params.size() > 0) {
            buffer.append('?');
            Iterator<QueryParam> it = params.iterator();
            while (it.hasNext()) {
                QueryParam p = it.next();
                buffer.append(p.name);
                buffer.append('=');
                buffer.append(p.value);
                if (it.hasNext()) {
                    buffer.append('&');
                }
            }

        }

        return buffer.toString();
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(final String extension) {
        this.extension = extension;
    }

    public void replace(final String s1, final String s2) {
        baseUrl = baseUrl.replace(s1, s2);
    }

    public void addParam(final QueryParam p) {
        params.add(p);
    }


}
